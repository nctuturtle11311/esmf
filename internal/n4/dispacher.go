package n4

func (s *N4Server) Dispatch(msg) {
	switch msg.Type {
	case PFCPAssociationRequest:
		s.p.HandlePFCPAssociationRequest(msg)
	}
}

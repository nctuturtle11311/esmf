package processor

import "esmf/internal/ctx"

type Processor struct {
	SMContext map[string]*ctx.SMContext
}

func NewProcessor() *Processor {
	handler := &Processor{}
	handler.init()

	return handler
}

func (h *Processor) init() {
	h.SMContext = make(map[string]*ctx.SMContext)
}

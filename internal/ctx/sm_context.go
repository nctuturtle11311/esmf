package ctx

type SMContext struct {
	Ref string

	UnauthenticatedSupi bool `json:"unauthenticatedSupi,omitempty"`

	Supi         string `json:"supi,omitempty"`
	Gpsi         string `json:"gpsi,omitempty"`
	Pei          string `json:"pei,omitempty"`
	Identifier   string `json:"id,omitempty"`
	PDUSessionID int32  `json:"pduSessionId,omitempty"`
	Dnn          string `json:"dnn,omitempty"`
}

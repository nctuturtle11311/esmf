package sbi

import (
	"esmf/internal/processor"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

type SBIServer struct {
	*http.Server
	router *gin.Engine

	processor *processor.Processor
}

func NewSBIServer() *SBIServer {
	s := &SBIServer{}
	s.init()

	s.Server = &http.Server{
		Addr:    "0.0.0.0:12345",
		Handler: s.router,
	}

	s.processor = processor.NewProcessor()

	return s
}

type Endpoint struct {
	Method  string
	Pattern string
	APIFunc gin.HandlerFunc
}

func (s *SBIServer) init() {
	s.router = gin.Default()
	var endpoints = []Endpoint{
		{
			Method:  strings.ToUpper("Post"),
			Pattern: "/sm-contexts",
			APIFunc: s.HTTPPostSmContexts,
		},
		{
			Method:  strings.ToUpper("Post"),
			Pattern: "/sm-contexts/:smContextRef/release",
			APIFunc: s.HTTPReleaseSmContext,
		},
		{
			Method:  strings.ToUpper("Post"),
			Pattern: "/sm-contexts/:smContextRef/modify",
			APIFunc: s.HTTPUpdateSmContext,
		},
		{
			Method:  strings.ToUpper("Post"),
			Pattern: "/sm-contexts/:smContextRef/retrieve",
			APIFunc: s.HTTPRetrieveSmContext,
		},
	}

	pduSessionGroup := s.router.Group("/nsmf-pdusession/v1")
	applyEndpoints(pduSessionGroup, endpoints)

}

func applyEndpoints(group *gin.RouterGroup, endpoints []Endpoint) {
	for _, endpoint := range endpoints {
		switch endpoint.Method {
		case "GET":
			group.GET(endpoint.Pattern, endpoint.APIFunc)
		case "POST":
			group.POST(endpoint.Pattern, endpoint.APIFunc)
		case "PUT":
			group.PUT(endpoint.Pattern, endpoint.APIFunc)
		case "DELETE":
			group.DELETE(endpoint.Pattern, endpoint.APIFunc)
		}
	}
}

func (s *SBIServer) ListenAndServe() error {
	return s.Server.ListenAndServe()
}

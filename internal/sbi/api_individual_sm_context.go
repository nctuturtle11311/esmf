package sbi

import (
	"github.com/gin-gonic/gin"
)

func (s *SBIServer) HTTPReleaseSmContext(c *gin.Context) {

}

func (s *SBIServer) HTTPRetrieveSmContext(c *gin.Context) {
	status, ret := s.processor.RetrieveSmContext(c.GetString("SmContextRef"))

	c.JSON(status, ret)
}

func (s *SBIServer) HTTPUpdateSmContext(c *gin.Context) {

}

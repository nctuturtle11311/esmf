package sbi

import (
	"github.com/gin-gonic/gin"
)

func (s *SBIServer) HTTPPostSmContexts(c *gin.Context) {
	s.processor.PostSmContext()
}

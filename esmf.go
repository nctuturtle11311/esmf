package esmf

import (
	"esmf/internal/sbi"
	"log"
)

type ESMF struct {
	sbiServer *sbi.SBIServer
}

func NewESMF() *ESMF {
	esmf := &ESMF{}

	esmf.sbiServer = sbi.NewSBIServer()

	return esmf
}

func (e *ESMF) Run() error {

	err := e.sbiServer.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	}

	return nil
}

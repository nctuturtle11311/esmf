package main

import (
	"esmf"
	"github.com/urfave/cli"
	"log"
	"os"
)

func main() {
	app := cli.NewApp()
	app.Name = "esmf"
	app.Action = action

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func action(c *cli.Context) {
	instance := esmf.NewESMF()

	err := instance.Run()

	if err != nil {
		log.Fatal(err)
	}
}

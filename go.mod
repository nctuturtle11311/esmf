module esmf

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/google/uuid v1.1.1
	github.com/urfave/cli v1.22.4
)
